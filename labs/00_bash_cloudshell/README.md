# Bash and CloudShell

*By the end of this lab, you will:*
1. Have Google Cloud Shell access
1. Be familiar with the Bash commands we'll use in this course
1. Have a "local" development environment set up

---

### Cloud Shell

Log in to [Google Cloud Console](https://console.cloud.google.com/)

Click the CloudShell icon

![image](gcp_menu.png?)

This will open a shell session in your browser.  The rest of this lab will be completed in the Cloud Shell.

Note: If you've used GCP Cloud Shell before, you may have some crusty leftover config in there.  We can probably troubleshoot through it, but you might just want to [reset your Cloud Shell](https://cloud.google.com/shell/docs/resetting-cloud-shell) and start fresh.

---

### Super-Helpful Resource

- [ExplainShell](https://explainshell.com) can break down and explain overwhelming or unclear shell commands. ([source](https://github.com/idank/explainshell))

---

### Filesystem, Directories, and Navigation

##### Make a new directory to house your coursework

```shell
mkdir ~/sfs
```

*Explanation:*
- `mkdir` is the command to make a new directory
- `~` is shorthand for your home directory: `/home/your_user_name`
- `sfs` is the new directory we're creating.  `~/sfs` is the full path of the new directory.  It extrapolates to `/home/your_user_name/sfs`

##### Navigate to the new directory

```shell
# Use the full path
cd ~/sfs
# or
cd /home/$USER/sfs
# or
cd $HOME/sfs

# Use the relative path
# First, make sure you're in your home directory
cd
# or
cd ~
# Second, use the relative path to get to your new directory
cd sfs
# or try
cd s # and before you hit 'enter', press your 'tab' key once or twice.
```

*Explanation:*
- You can use the full path (as long as it's correct) to get to any directory on your filesystem
- A relative path is ... relative ... to your current location in the filesystem
- Tab autocompletion is fantastic.  Play with it.
- Note that the shell environment gives us a bunch of variables by default.  `$USER` is one such variable.  You can see others with the `env` command.

*A Quick Note on Variables:*
- `$` indicates that we want the *value* of the variable here.
- `NAME=Frodo` sets `$NAME` to `Frodo`
- Unlike many programming languages, most shells require **no spaces** around the `=`

##### Examine the new directory

```shell
# List the contents of the directory
ls
# And again in 'list' (-l) format
ls -l
# And again showing a 'list' (-l) of 'all' (-a) files (including normally hidden files)
ls -la
```

*Explanation:*
- With the `ls -la` command options, you'll see `.` and `..`
- `.` is shorthand for "current directory"
- `..` is shorthand for the parent directory

---

### Environment Management

##### Shell-wide environment

When you want a configuration available to you no matter where you are on the system, you can set an environment variable in your `~/.bashrc` file.  Because this file is loaded every time you log in, you'll always have it available.

We're all going to pick a unique [MacGuffin](https://en.wikipedia.org/wiki/MacGuffin) that will identify resources we create in our shared environment.

```shell
# Choose a unique MacGuffin to identify your resources.
# To be safe, use all lower case letters (no numbers or special chars)
echo 'export MACGUFFIN=maltesefalcon' >> ~/.bashrc
# EDIT this expression before running it:
# - remove the '# ' at the beginning
# - replace 'maltesefalcon' with your unique MacGuffin: 'ring', 'rug', 'time', or whatever
# echo "export MACGUFFIN='maltesefalcon'" >> ~/.bashrc

# Tell your shell to re-read your .bashrc file
source ~/.bashrc

# Verify
echo $MACGUFFIN
```
 
<!--
##### Application-specific environment file

Sometimes you need a local config to work with an otherwise cloud-based application.  Adding a local environment file can be very helpful.

```shell
# Create an environment file with a sample variable
echo "SAMPLE_VARIABLE='Hello World'" > ~/sfs/.env

# Append another variable to the file (note the ">>" instead of ">")
echo "ENVIRONMENT='local'" >> ~/sfs/.env

# Examine the file
cat ~/sfs/.env

# Load the file into your shell environment
source ~/sfs/.env

# Examine the variable you set
echo $SAMPLE_VARIABLE
```

##### Edit the environment file

```shell
# Open the environment file in the default editor
edit ~/sfs/.env
```

Add a new line with the text `MYVAR='Another One'`, then save and close the file.

```shell
# Examine the new variable
echo $MYVAR

# It's not there because we didn't re-read the file for the shell
source ~/sfs/.env

# Try again
echo $MYVAR
```

We can use this file to store any environmental config we need for the rest of our labs.
-->


---

##### Logout

- Pressing CTRL+D will log you out of a shell

---

##### End of Lab

---

### Self-Study and/or Extra Credit

### More notes on directories and the filesystem

##### An Unusual Class Convention

In this class, you will find instructions like this:

```shell
touch ~/sfs/mywebserver/deploy/Chart.yaml
edit ~/sfs/mywebserver/deploy/Chart.yaml
```

We do this because it's exceptionally clear, and it will work even if you've wandered into a different directory at some point.  If you prefer to type instead of copy/paste, something like this will yield the same results:

```shell
cd ~/sfs/mywebserver/deploy
touch Chart.yaml
edit Chart.yaml
```

##### Tildae
- In Bash, `~` is a shortcut meaning "this user's home directory"
- So when the user `fbaggins` is logged in to a system, `mkdir ~/sfs` is a shortcut for `mkdir /home/fbaggins/sfs`.  (Probably.  Frodo's home directory could be somewhere else.)

##### Paths
`/my_folder/my_file` vs. `my_folder/my_file`
- In a Bash shell, the `/` directory is the root of the filesystem.  Much like `C:\` on Windows.
- `/my_folder/my_file` is an absolute path to `my_file`
- `my_folder/myfile` is a relative path, which means...
  - If you're in `/tmp`:
    - `my_folder/my_file` is actually `/tmp/my_folder/my_file`
  - If you're in `/home/fbaggins`:
    - `my_folder/my_file` is actually `/home/fbaggins/my_folder/my_file`
  - etc.

### Command tricks

##### Autocomplete

- Try using the `Tab` key to autocomplete paths or commands
  - Type `cd ~/sf` without pressing Enter.  Press `Tab` instead.
  - Try typing `echo $MACG` and pressing `Tab`.

##### Command history

- You can type the up and down arrows to cycle through previous commands
- Bash variables
  - `!!` will re-run the last command
  - `!$` or `$_` will insert the last argument of the previous command:

    ```shell
    touch Chart.yaml
    edit !$
    ```

  - `!<search_string>` will immediately re-run the most recent command **starting** with `<search_string>` (risky, but handy for non-destructive things like `!edit` or `!vim`)
  - `!?<search_string>` will immediately re-run the most recent command **containing**  `<search_string>` (risky)
  - Some shells (like ZSH) will help protect you by expanding these found commands at the prompt; you have to hit Enter again to run them.
- CTRL+R
  - To search for and re-run a previous command, press CTRL+R, then type part of the command you want to re-run
  - A matching command should appear
  - Press CTRL+R again to cycle through earlier matching commands

##### Command substitution

- The `$()` syntax tells the shell to substitute the output of the command in place of the entire `$(...)` string
- `echo which date` will output `which date`
- `echo $(which date)` causes `which date` to be executed first, which results in `echo /bin/date`
- `echo $($(which date))` causes `which date` to be executed first, which results in the date being printed

```shell
date
# Wed Nov 30 14:34:08 MST 2016
echo which date
# which date
echo $(which date)
# /bin/date
echo $($(which date))
# Wed Nov 30 14:34:08 MST 2016
```

---

|Next: [GitLab](/labs/01_gitlab)|
|:---:|
